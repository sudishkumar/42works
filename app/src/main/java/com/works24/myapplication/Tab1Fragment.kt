package com.works24.myapplication

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.works24.myapplication.DataList.names


class Tab1Fragment() : Fragment(), OnItemClickListener  {

    lateinit var availableAccountAdapter: RecyclerAdapter
    lateinit var tab1RecyclerView: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tab1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tab1RecyclerView = view.findViewById(R.id.tab1RecyclerView)
        DataList.tab1FragmentList = tab1RecyclerView

    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onClick(pos: Int, checkBox: Boolean) {
        val data = names
        data[pos] = DataModel(data[pos].name , checkBox)

        DataList.tab2FragmentList?.adapter?.notifyDataSetChanged()


    }
    @SuppressLint("NotifyDataSetChanged")
    override fun onResume() {
        super.onResume()

        availableAccountAdapter = RecyclerAdapter(names, this)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        tab1RecyclerView.layoutManager = layoutManager
        tab1RecyclerView.adapter = availableAccountAdapter
        availableAccountAdapter.notifyDataSetChanged()

    }
}