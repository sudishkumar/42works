package com.works24.myapplication

import androidx.recyclerview.widget.RecyclerView

object DataList {
    val names = arrayListOf<DataModel>( DataModel("Sudish", false), DataModel("Ravi", false), DataModel("Chanchal", false), DataModel("Shrawan", false), DataModel("Ansar", false), DataModel("Arman", false))
    var tab1FragmentList: RecyclerView ? = null
    var tab2FragmentList: RecyclerView ? = null
}
