package com.works24.myapplication

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

 class TabsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(poasition: Int): CharSequence? {
        when (poasition) {
            0 -> {
                return "Tab1"
            }
            1 -> {
                return " Tab2 "
            }

        }
        return super.getPageTitle(poasition)
    }

    override fun getItem(poasition: Int): Fragment {

        return when (poasition) {
            0 -> {
                Tab1Fragment()
            }

            else -> {
                Tab2Fragment()
            }

        }

    }
}