package com.works24.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.works24.myapplication.DataList.names


class Tab2Fragment() : Fragment(), OnItemClickListener {
     lateinit var recyclerAdapter: RecyclerAdapter
     lateinit var tab2RecyclerView: RecyclerView;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tab2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tab2RecyclerView = view.findViewById(R.id.tab2RecyclerView)
        DataList.tab2FragmentList = tab2RecyclerView;
    }

    override fun onResume() {
        super.onResume()

       recyclerAdapter = RecyclerAdapter(names, this)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        tab2RecyclerView.layoutManager = layoutManager
        tab2RecyclerView.adapter = recyclerAdapter
        recyclerAdapter.notifyDataSetChanged()
    }
    override fun onClick(pos: Int, checkBox: Boolean) {
        val data = names
        data[pos] = DataModel(data[pos].name , checkBox)

        DataList.tab1FragmentList?.adapter?.notifyDataSetChanged()

    }


}