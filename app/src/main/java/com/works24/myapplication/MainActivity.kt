package com.works24.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewPager = findViewById<ViewPager>(R.id.viewPager_account)
        val tabLayout = findViewById<TabLayout>(R.id.tabLayout_account)

        viewPager.adapter = TabsAdapter(supportFragmentManager)

        tabLayout.setupWithViewPager(viewPager)

    }
}