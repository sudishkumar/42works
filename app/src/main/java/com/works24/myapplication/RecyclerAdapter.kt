package com.works24.myapplication

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter(var dataset : List<DataModel>,  val onItemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.tab1_item_layout , parent,false)

        return ViewHolder(view, onItemClickListener)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(dataset[position])
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    class ViewHolder(itemView: View, onItemClickListener: OnItemClickListener) : RecyclerView.ViewHolder(itemView, ){

        init {
            val checkBox =itemView.findViewById<CheckBox>(R.id.checkbox)
            checkBox.setOnClickListener {
                if (checkBox.isChecked){
                    onItemClickListener.onClick(position, true )
                }else{
                    onItemClickListener.onClick(position, false)
                }

            }
        }
        @SuppressLint("SetTextI18n")
        fun bind(dataModel: DataModel) {

           val name = itemView.findViewById<TextView>(R.id.name)
           val checkBox = itemView.findViewById<CheckBox>(R.id.checkbox)
            name.text = dataModel.name
            checkBox.isChecked = dataModel.checkBoxStatus

        }

        }
    }


interface OnItemClickListener{
    fun onClick(name: Int, checkBox: Boolean)
}